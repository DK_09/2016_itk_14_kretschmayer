﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormCalculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void x1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Add_Click(object sender, EventArgs e)
        {
            Vektor v1 = new Vektor(Convert.ToInt32(x1.Text), Convert.ToInt32(y1.Text));

            Vektor v2 = new Vektor(Convert.ToInt32(x2.Text), Convert.ToInt32(y2.Text));

            Vektor vErg = AddVector(v1, v2);
            

            MessageBox.Show(vErg.ToString());
        }

        static Vektor AddVector(Vektor v, Vektor v2)
        {
            Vektor vErgebniss = new Vektor(0, 0);

            vErgebniss.X = v.X + v2.X;
            vErgebniss.Y = v.Y + v2.Y;

            return vErgebniss;
        }
    }
}
