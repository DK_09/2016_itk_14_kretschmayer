﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vector2D_3D
{
    class Vektor2D
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Vektor2D(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

        public void VektorAddition(Vektor2D vektor)
        {
            this.X = this.X + vektor.X;
            this.Y = this.Y + vektor.Y;
        }

        public virtual double GetLänge()
        {
            double d = 0;

            d = Math.Sqrt(Math.Pow(this.X, 2) + Math.Pow(this.Y, 2));

            return d;
        }
    }
}
