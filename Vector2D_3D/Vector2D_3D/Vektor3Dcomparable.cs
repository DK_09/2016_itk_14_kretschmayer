﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vector2D_3D
{
    class Vektor3Dcomparable : Vektor3D , IComparable
    {
        public Vektor3Dcomparable(float x, float y, float z)
            : base(x, y, z)
        { 
        
        }

        public int CompareTo(object o)
        {
            return this.GetLänge().CompareTo(((Vektor3Dcomparable)o).GetLänge());
        }

        public object Clone()
        {
            return new Vektor3Dcomparable(this.X, this.Y, this.Z);
        }
    }
}
