﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vector2D_3D
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.
            List<Vektor3D> list3d = new List<Vektor3D>();

            Vektor2D v1 = new Vektor2D(3, 6);
            Vektor2D v2 = new Vektor2D(4, 8);
            Vektor2D v3 = new Vektor2D(5, 10);

            Drei2DVektoren(v1, v2, v3);
            Console.WriteLine("");

            //3.
            /*
            Vektor3D v4 = new Vektor3D(2, 54, 23);
            Vektor3D v5 = new Vektor3D(5, 83, 21);
            Vektor3D v6 = new Vektor3D(7, 11, 87);
            Vektor3D v7 = new Vektor3D(5, 83, 21);
            Vektor3D v8 = new Vektor3D(7, 11, 87);

            list3d.Add(v4);
            list3d.Add(v5);
            list3d.Add(v6);
            list3d.Add(v7);
            list3d.Add(v8);

            Drei3DVektoren(v4, v5, v6);

            list3d.Sort();

            foreach (Vektor3D v in list3d)
            {
                Console.WriteLine(v.ToString());
            }
            */
            //4.

            Vektor3Dcomparable v4 = new Vektor3Dcomparable(12, 34, 45);
            Vektor3Dcomparable v5 = new Vektor3Dcomparable(12, 34, 45);
            Vektor3Dcomparable v6 = new Vektor3Dcomparable(12, 34, 45);

            Vektor3Dcomparable v7 = new Vektor3Dcomparable(12, 34, 45);
            Vektor3Dcomparable v8 = (Vektor3Dcomparable)v7.Clone();
            Vektor3Dcomparable v9 = (Vektor3Dcomparable)v7.Clone();

            Drei3DVektoren(v4, v5, v6);

            Drei3DVektoren(v7, v8, v9);

            Console.ReadLine();
        }

        static void Drei2DVektoren(Vektor2D v1, Vektor2D v2, Vektor2D v3)
        {
            v2.VektorAddition(v3);
            v1.VektorAddition(v2);
            Console.WriteLine("Addition: \t"+ v1.X + " " + v1.Y);
            Console.WriteLine("Gesamtlänge: \t" + v1.GetLänge());
        }
        static void Drei3DVektoren(Vektor3D v1, Vektor3D v2, Vektor3D v3)
        {
            v2.VektorAddition(v3);
            v1.VektorAddition(v2);
            Console.WriteLine("Addition: \t" + v1.X + " " + v1.Y + " " + v1.Z);
            Console.WriteLine("Gesamtlänge: \t" + v1.GetLänge());
        }
    }
}
