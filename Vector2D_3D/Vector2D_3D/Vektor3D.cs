﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vector2D_3D
{
    class Vektor3D : Vektor2D
    {
        public float Z { get; set; }

        public Vektor3D(float x, float y, float z)
            : base(x, y)
        {
            this.Z = z;
        }

        public void VektorAddition(Vektor3D vektor)
        {
            this.X = this.X + vektor.X;
            this.Y = this.Y + vektor.Y;
            this.Z = this.Z + vektor.Z;
           // base.VektorAddition(vektor);
        }

        public override double GetLänge()
        {
            double d = 0;

            d = Math.Sqrt(Math.Pow(this.X, 2) + Math.Pow(this.Y, 2) + Math.Pow(this.Z, 2));

            return d;
            //return base.GetLänge();
        }

        public override string ToString()
        {
            return "3DVektor (" + this.X + " " + this.Y + " " + this.Z + ")";

            //return base.ToString();
        }
    }
}
