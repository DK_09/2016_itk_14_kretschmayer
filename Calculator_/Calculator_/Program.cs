﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector v = new Vector(0, 0);
            Vector v2 = new Vector(0, 0);

            Console.WriteLine("Geben Sie die Werte des erstes Vektors ein: \n");
            Console.WriteLine("X: ");
            v.X = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Y: ");
            v.Y = Convert.ToInt32(Console.ReadLine());



            Console.WriteLine("Geben Sie die Werte des zweiten Vektors ein: \n");
            Console.WriteLine("X: ");
            v2.X = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Y: ");
            v2.Y = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("\n\n\nErgebnis: " + AddVector(v, v2).ToString());

            Console.ReadLine();
        }

        static Vector AddVector(Vector v, Vector v2)
        {
            Vector vErgebniss = new Vector(0, 0);

            vErgebniss.X = v.X + v2.X;
            vErgebniss.Y = v.Y + v2.Y;

            return vErgebniss;
        }


    }
}
