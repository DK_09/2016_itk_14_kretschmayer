﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AverageException
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> lint = new List<int> { 1, 33, 3, 7, 5};
            List<int> nint = new List<int> { };
            double av = 0;
            string s = "Problem detected!";
            //Console.WriteLine(average(lint));
            
            //Console.WriteLine(average(nint));

            if (averageTryParse(nint, out av))
            {
                try
                {
                    Console.WriteLine(average(nint));
                }
                catch(Exception ex) 
                {
                    Console.WriteLine("Information:\t" + ex.Message);
                }
            }
            else
            {
                Exception ex = MyFirstException(s);

                Console.WriteLine("Message: {0}", ex.Message);
                Console.WriteLine("{0} {1}", ex.Data["InfoM"], ex.Data["InfoN"]);
                Console.WriteLine("{0} {1}", ex.Data["Info"], ex.Data["Date"]);   
            }

            Console.ReadLine();
        }

        static double average(List<int> lint)
        {
            double gesamt = 0;
            double anzahl = lint.Count;
            double proz = 0;


            foreach (int i in lint)
            {
                gesamt = gesamt + i;
            }

            try
            {
                proz = gesamt / anzahl;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Information:\t" + ex.Message);
            }

            return proz;
        }

        static bool averageTryParse(List<int> l, out double av)
        {
            double gesamt = 0;
            double anzahl = l.Count;
            av = 0;


            foreach (int i in l)
            {
                gesamt = gesamt + i;
            }

            av = gesamt / anzahl;
           


            if (l.Count == 0)
            {
                return false;
            }
            else
                return true;

        }

        static Exception MyFirstException(string s)
        {
            Exception ex = new Exception();


            ex.Data.Add("InfoM", "Information:");
            ex.Data.Add("InfoN", s);

            ex.Data.Add("Info", "Datum/Zeit");
            ex.Data.Add("Date", DateTime.Now);


            return ex;
        }
    }
}
