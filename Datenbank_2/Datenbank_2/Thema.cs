﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datenbank_2
{
    class Thema
    {
        public Thema(int id, string bezeichnung)
        {
            this.ID = id;
            this.Bezeichnung = bezeichnung;
        }

        public override string ToString()
        {
            return Bezeichnung;
        }

        public int ID { get; set; }
        public string Bezeichnung { get; set; }
    }
}
