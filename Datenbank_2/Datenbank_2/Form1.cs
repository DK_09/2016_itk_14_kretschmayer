﻿/*-------------------------------------------------------------
* Klasse: Datenbank_II (Form1)
* Autor: Ich
* Datum: 29.04.2016 (Eigentlich 28.04.2016, aber kleinere Änderungen sind dazugekommen z.B.: Design)
* Beschreibung: Diese Form soll aus Themen die passenden Bücher finden. Diese Bücher kann man dann auch bearbeiten und werden in die Datenbank gespeichert (überschrieben).
*
-------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Datenbank_2
{
    public partial class Form1 : Form
    {

       // string conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb";

        OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb");
    
        public Form1()
        {
            InitializeComponent();

            //string conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb";

            //OleDbConnection con = new OleDbConnection(conStr);

            try
            {
                con.Open();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten!\nGenauere Informationen: "+ex.Message);
            }
            Themen_befüllen(con);
        }

        internal void Themen_befüllen(OleDbConnection con)
        {
            List<Thema> t_liste = new List<Thema>();

            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select ID, Bezeichnung from themengruppe";

            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                t_liste.Add(new Thema(reader.GetInt32(0), reader.GetString(1)));
            }
            reader.Close();


            t_liste.Sort(delegate(Thema t1, Thema t2)
            {
                return t1.ID.CompareTo(t2.ID);
            });

            foreach (Thema t in t_liste)
            {
                listbox_themen.Items.Add(t.ToString());
            }
        }

        private void listbox_themen_SelectedIndexChanged(object sender, EventArgs e)
        {
            listbox_buch.Items.Clear();


            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT titel from buch WHERE themenid = (SELECT id from themengruppe WHERE bezeichnung = '" + (string)listbox_themen.SelectedItem + "');";

            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                listbox_buch.Items.Add(reader.GetString(0));
            }
            reader.Close();
        }

        private void listbox_buch_SelectedIndexChanged(object sender, EventArgs e)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT titel, autor, erscheinungsjahr, isbn from buch WHERE titel = '" + (string)listbox_buch.SelectedItem + "';";

            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                textBox_titel.Text = reader.GetString(0);
                textBox_autor.Text = reader.GetString(1);
                textBox_jahr.Text =  Convert.ToString(reader.GetValue(2));
                textBox_isbn.Text = reader.GetString(3);
            }
            reader.Close();
        }

        private void ändern_Click(object sender, EventArgs e)
        {

            if (textBox_titel.Text == "" || textBox_autor.Text == "" || textBox_isbn.Text == "")
            {
                MessageBox.Show("Bitte geben Sie gültige Werte ein!");
            }
            else
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;
                cmd.CommandText = "UPDATE buch SET titel='" + textBox_titel.Text + "', autor='" + textBox_autor.Text + "', erscheinungsjahr='" + textBox_jahr.Text + "', isbn='" + textBox_isbn.Text + "' WHERE titel='" + (string)listbox_buch.SelectedItem + "'";

                int changed = cmd.ExecuteNonQuery();

                if (changed == 1)
                {
                    MessageBox.Show("Der Eintrag wurde erfolgreich geändert!");
                }
                else
                    MessageBox.Show("Es wurde nichts verändert!");
            }
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            con.Close();
        }

        private void textBox_titel_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}
