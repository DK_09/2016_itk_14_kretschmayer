﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datenbank_2
{
    class Buch
    {
        public Buch(string titel, int themenid)
        {
            this.Titel = titel;
            this.ThemenID = themenid;
        }

        public int Buchnr { get; set; }
        public int ThemenID { get; set; }
        public string ISBN { get; set; }
        public string Titel { get; set; }
        public string Autor { get; set; }
        public int Datum { get; set; }

        public override string ToString()
        {
            return Titel;
        }

    }
}
