﻿namespace Datenbank_2
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_themen = new System.Windows.Forms.Label();
            this.label_buch = new System.Windows.Forms.Label();
            this.listbox_themen = new System.Windows.Forms.ListBox();
            this.listbox_buch = new System.Windows.Forms.ListBox();
            this.ändern = new System.Windows.Forms.Button();
            this.label_titel = new System.Windows.Forms.Label();
            this.label_autor = new System.Windows.Forms.Label();
            this.label_jahr = new System.Windows.Forms.Label();
            this.label_isbn = new System.Windows.Forms.Label();
            this.textBox_titel = new System.Windows.Forms.TextBox();
            this.textBox_autor = new System.Windows.Forms.TextBox();
            this.textBox_jahr = new System.Windows.Forms.TextBox();
            this.textBox_isbn = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label_themen
            // 
            this.label_themen.AutoSize = true;
            this.label_themen.Location = new System.Drawing.Point(54, 55);
            this.label_themen.Name = "label_themen";
            this.label_themen.Size = new System.Drawing.Size(49, 13);
            this.label_themen.TabIndex = 0;
            this.label_themen.Text = "Themen:";
            // 
            // label_buch
            // 
            this.label_buch.AutoSize = true;
            this.label_buch.Location = new System.Drawing.Point(404, 55);
            this.label_buch.Name = "label_buch";
            this.label_buch.Size = new System.Drawing.Size(35, 13);
            this.label_buch.TabIndex = 1;
            this.label_buch.Text = "Buch:";
            // 
            // listbox_themen
            // 
            this.listbox_themen.FormattingEnabled = true;
            this.listbox_themen.Location = new System.Drawing.Point(57, 89);
            this.listbox_themen.Name = "listbox_themen";
            this.listbox_themen.Size = new System.Drawing.Size(274, 394);
            this.listbox_themen.TabIndex = 2;
            this.listbox_themen.SelectedIndexChanged += new System.EventHandler(this.listbox_themen_SelectedIndexChanged);
            // 
            // listbox_buch
            // 
            this.listbox_buch.FormattingEnabled = true;
            this.listbox_buch.Location = new System.Drawing.Point(407, 89);
            this.listbox_buch.Name = "listbox_buch";
            this.listbox_buch.Size = new System.Drawing.Size(274, 394);
            this.listbox_buch.TabIndex = 3;
            this.listbox_buch.SelectedIndexChanged += new System.EventHandler(this.listbox_buch_SelectedIndexChanged);
            // 
            // ändern
            // 
            this.ändern.Location = new System.Drawing.Point(847, 387);
            this.ändern.Name = "ändern";
            this.ändern.Size = new System.Drawing.Size(160, 47);
            this.ändern.TabIndex = 4;
            this.ändern.Text = "Ändern";
            this.ändern.UseVisualStyleBackColor = true;
            this.ändern.Click += new System.EventHandler(this.ändern_Click);
            // 
            // label_titel
            // 
            this.label_titel.AutoSize = true;
            this.label_titel.Location = new System.Drawing.Point(771, 148);
            this.label_titel.Name = "label_titel";
            this.label_titel.Size = new System.Drawing.Size(30, 13);
            this.label_titel.TabIndex = 5;
            this.label_titel.Text = "Titel:";
            // 
            // label_autor
            // 
            this.label_autor.AutoSize = true;
            this.label_autor.Location = new System.Drawing.Point(771, 199);
            this.label_autor.Name = "label_autor";
            this.label_autor.Size = new System.Drawing.Size(35, 13);
            this.label_autor.TabIndex = 6;
            this.label_autor.Text = "Autor:";
            // 
            // label_jahr
            // 
            this.label_jahr.AutoSize = true;
            this.label_jahr.Location = new System.Drawing.Point(771, 250);
            this.label_jahr.Name = "label_jahr";
            this.label_jahr.Size = new System.Drawing.Size(30, 13);
            this.label_jahr.TabIndex = 7;
            this.label_jahr.Text = "Jahr:";
            // 
            // label_isbn
            // 
            this.label_isbn.AutoSize = true;
            this.label_isbn.Location = new System.Drawing.Point(771, 305);
            this.label_isbn.Name = "label_isbn";
            this.label_isbn.Size = new System.Drawing.Size(35, 13);
            this.label_isbn.TabIndex = 8;
            this.label_isbn.Text = "ISBN:";
            // 
            // textBox_titel
            // 
            this.textBox_titel.Location = new System.Drawing.Point(874, 145);
            this.textBox_titel.Name = "textBox_titel";
            this.textBox_titel.Size = new System.Drawing.Size(100, 20);
            this.textBox_titel.TabIndex = 9;
            this.textBox_titel.TextChanged += new System.EventHandler(this.textBox_titel_TextChanged);
            // 
            // textBox_autor
            // 
            this.textBox_autor.Location = new System.Drawing.Point(874, 196);
            this.textBox_autor.Name = "textBox_autor";
            this.textBox_autor.Size = new System.Drawing.Size(100, 20);
            this.textBox_autor.TabIndex = 10;
            // 
            // textBox_jahr
            // 
            this.textBox_jahr.Location = new System.Drawing.Point(874, 247);
            this.textBox_jahr.Name = "textBox_jahr";
            this.textBox_jahr.Size = new System.Drawing.Size(100, 20);
            this.textBox_jahr.TabIndex = 11;
            // 
            // textBox_isbn
            // 
            this.textBox_isbn.Location = new System.Drawing.Point(874, 302);
            this.textBox_isbn.Name = "textBox_isbn";
            this.textBox_isbn.Size = new System.Drawing.Size(100, 20);
            this.textBox_isbn.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1179, 571);
            this.Controls.Add(this.textBox_isbn);
            this.Controls.Add(this.textBox_jahr);
            this.Controls.Add(this.textBox_autor);
            this.Controls.Add(this.textBox_titel);
            this.Controls.Add(this.label_isbn);
            this.Controls.Add(this.label_jahr);
            this.Controls.Add(this.label_autor);
            this.Controls.Add(this.label_titel);
            this.Controls.Add(this.ändern);
            this.Controls.Add(this.listbox_buch);
            this.Controls.Add(this.listbox_themen);
            this.Controls.Add(this.label_buch);
            this.Controls.Add(this.label_themen);
            this.Name = "Form1";
            this.Text = "Datenbank_II";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_themen;
        private System.Windows.Forms.Label label_buch;
        private System.Windows.Forms.ListBox listbox_themen;
        private System.Windows.Forms.ListBox listbox_buch;
        private System.Windows.Forms.Button ändern;
        private System.Windows.Forms.Label label_titel;
        private System.Windows.Forms.Label label_autor;
        private System.Windows.Forms.Label label_jahr;
        private System.Windows.Forms.Label label_isbn;
        private System.Windows.Forms.TextBox textBox_titel;
        private System.Windows.Forms.TextBox textBox_autor;
        private System.Windows.Forms.TextBox textBox_jahr;
        private System.Windows.Forms.TextBox textBox_isbn;
    }
}

