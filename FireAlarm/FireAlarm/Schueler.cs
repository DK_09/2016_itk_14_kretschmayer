﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireAlarm
{
    class Schueler
    {

        private string name;
        private Klasse klasse;
        private Schule schule;

        public Schueler(string n, Schule s, Klasse k)
        {
            name = n;
            schule = s;
            klasse = k;

            schule.fire += s_fire;
        }


        public void s_fire(string typ)
        {
            string atyp = typ.ToUpper();
            Console.WriteLine(atyp+"!! Schüler: '"+name+"' begibt sich in den Hof "+schule.GetPlatzNummer(klasse));
        }
    }
}
