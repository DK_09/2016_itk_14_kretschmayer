﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireAlarm
{
    class Schule
    {
        public delegate void DelTyp(string typ);
        public event DelTyp fire;
        public Schule()
        { 
        
        }

        public int GetPlatzNummer(Klasse k)
        {
            int platz = 0;

            switch (k.Name)
            { 
                case "3BHITT":
                    platz = 1;
                    break;

                case "2BHITT":
                    platz = 2;
                    break;

                default:
                    platz = 5;
                    break;
            }

            return platz;
        }

        public void StartFeueralarm(string typ)
        {
            fire(typ);
        }
    }
}
