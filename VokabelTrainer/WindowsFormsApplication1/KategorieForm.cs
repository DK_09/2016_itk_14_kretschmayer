﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace WindowsFormsApplication1
{
    public partial class KategorieForm : Form
    {
        public KategorieForm()
        {
            InitializeComponent();

            Aktualisieren();
        }


        private void button_erstellen_Click(object sender, EventArgs e)
        {
            OleDbCommand cmd = new OleDbCommand("SELECT MAX(id) FROM kategorie", Connection.GetConnection().DBConnection);

            int zahl = (int)cmd.ExecuteScalar();


            textBox_ID.Text = Convert.ToString(zahl + 1);

            listBox_Kategorien.ClearSelected();

            Connection.GetConnection().isDirty = false;


            richTextBox_Beschreibung.ReadOnly = false;
            richTextBox_Beschreibung.Clear();
            richTextBox_Beschreibung.BackColor = Color.White;

        }

        private void listBox_Kategorien_SelectedIndexChanged(object sender, EventArgs e)
        {
            richTextBox_Beschreibung.ReadOnly = true;
            richTextBox_Beschreibung.BackColor = Color.LightGray;

            try
            {
                Kategorie k = (Kategorie)listBox_Kategorien.SelectedItem;

                textBox_ID.Text = Convert.ToString(k.ID);
                richTextBox_Beschreibung.Text = k.Bereich;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void button_ändern_Click(object sender, EventArgs e)
        {
            richTextBox_Beschreibung.ReadOnly = false;
            richTextBox_Beschreibung.BackColor = Color.White;

            Connection.GetConnection().isDirty = true;
        }

        private void button_löschen_Click(object sender, EventArgs e)
        {
            OleDbCommand cmd;
            int iadsf = 0;

            Kategorie k = (Kategorie)listBox_Kategorien.SelectedItem;

            cmd = new OleDbCommand("SELECT v.id FROM vokabel v WHERE v.kat = "+k.ID+";", Connection.GetConnection().DBConnection);

            try
            {
                iadsf = (int)cmd.ExecuteScalar();
            }
            catch
            {
                iadsf = 0;
            }

            

            if (iadsf != 0)
            {
                MessageBox.Show("Es sind noch immer Vokabeln in dieser Kategorie, daher konnte diese nicht gelöscht werden!");
            }
            else
            {
                cmd = new OleDbCommand("DELETE FROM kategorie WHERE id = " + k.ID + ";", Connection.GetConnection().DBConnection);

                int i = cmd.ExecuteNonQuery();

                MessageBox.Show("" + i);

                Aktualisieren();
            }
        }

        private void button_speichern_Click(object sender, EventArgs e)
        {
            OleDbCommand cmd;
            Kategorie k = (Kategorie)listBox_Kategorien.SelectedItem;
            int i = 0;
            string s = textBox_ID.Text;


            if (s != "" && s!=null)
            {
                cmd = new OleDbCommand("SELECT id FROM kategorie WHERE bereich = '" + richTextBox_Beschreibung.Text + "'", Connection.GetConnection().DBConnection);

                int o = (int)cmd.ExecuteScalar();

                if (o == Convert.ToInt32(s))
                {
                    MessageBox.Show("Geht!");

                    if (Connection.GetConnection().isDirty)
                    {
                        Connection.GetConnection().isDirty = false;

                        cmd = new OleDbCommand("UPDATE kategorie SET bereich = '" + richTextBox_Beschreibung.Text + "' WHERE id = " + k.ID + ";", Connection.GetConnection().DBConnection);

                        i = cmd.ExecuteNonQuery();

                        MessageBox.Show("" + i);
                    }
                    else
                    {
                        int id = Convert.ToInt32(textBox_ID.Text);
                        string bereich = richTextBox_Beschreibung.Text;

                        cmd = new OleDbCommand("INSERT INTO kategorie VALUES (" + id + ",'" + bereich + "');", Connection.GetConnection().DBConnection);

                        i = cmd.ExecuteNonQuery();

                        MessageBox.Show("" + i);
                    }
                }
                else
                {
                    MessageBox.Show("Hell no!");
                }

            }
            else
            {
                MessageBox.Show("Geben Sie bitte einen gültigen Wert ein!");
            }




            Aktualisieren();

        }



        public void Aktualisieren()
        {
            listBox_Kategorien.Items.Clear();


            OleDbCommand cmd = new OleDbCommand("SELECT id, bereich FROM kategorie", Connection.GetConnection().DBConnection);

            List<Kategorie> klist = new List<Kategorie>();

            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                klist.Add(new Kategorie(reader.GetInt32(0), reader.GetString(1)));
            }

            reader.Close();


            foreach (Kategorie k in klist)
            {
                listBox_Kategorien.Items.Add(k);
            }

            textBox_ID.Clear();
            textBox_ID.ReadOnly = true;

            richTextBox_Beschreibung.Clear();
            richTextBox_Beschreibung.ReadOnly = true;
            richTextBox_Beschreibung.BackColor = Color.LightGray;


        }
    }
}
