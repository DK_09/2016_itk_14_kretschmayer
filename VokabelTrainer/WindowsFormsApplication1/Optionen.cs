﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.OleDb;

namespace WindowsFormsApplication1
{
    class Optionen
    {
        public bool isDirty { get; set; }
        public int numValue { get; set; }
        public bool mod { get; set; }
        public string smod { get; set; }
        public List<Beurteilung> blist = new List<Beurteilung>();
        public int testanzahl { get; set; }
        public string CurrentFilename { get; set; }
        public bool Panelhier { get; set; }
        public List<Vokabeln> ListTempVok { get; set; }
        //public bool confbool { get; set; }

        private static Optionen opt = null;

        private Optionen() { }

        public static Optionen GetOption()
        {
            if (opt == null)
            {
                opt = new Optionen();
            }

            return opt;
         }

        public void ReadOptions()
        {
            if (!File.Exists("config.txt"))
            {
                File.Create("config.txt");
            }

            StreamReader sr = new StreamReader("config.txt");

            int anzahl = testanzahl;
            string modus = "";
            bool modusB = false;

            int prozent = 0;
            string bname = "";

            while (sr.Peek() != -1)
            {
                string blabla = sr.ReadLine();

                if (blabla.Contains(";"))
                {
                    string[] s = blabla.Split(';');

                    anzahl = Convert.ToInt32(s[0]);
                    modus = s[1];
                }

                if (blabla.Contains("#"))
                {
                    string[] s2 = blabla.Split('#');

                    prozent = Convert.ToInt32(s2[0]);
                    bname = s2[1];

                    blist.Add(new Beurteilung(prozent, bname));
                }
            }

            sr.Close();

            if (modus.ToLower() == "true")
            {
                modusB = true;
                smod = "EN-DE";
            }
            else
            {
             modusB = false;
             smod = "DE-EN";
            }
            
            if (numValue != anzahl || mod != modusB)
            {
                isDirty = true;
            }
            else
                isDirty = false;


            //die ganzen variablen

            numValue = anzahl;
        }

        public void WriteOptions(bool isDirty)
        {
            bool modusauswahl = false;

            modusauswahl = mod;

            if (File.Exists("config.txt"))
            {
                File.Delete("config.txt");
            }

            StreamWriter sw = new StreamWriter("config.txt");

            if (isDirty == true)
            {
                sw.WriteLine(numValue + ";" + modusauswahl);

            }
            else
            { }



            foreach (Beurteilung b in blist)
            {
                if (b.Beurteilungname == "" || b.Beurteilungname == null)
                {

                }
                else
                {
                    sw.WriteLine(b.Prozent + "#" + b.Beurteilungname);
                }
            }

            sw.Flush();
            sw.Close();
        }

        public List<Vokabeln> GibVokabelList(int kat)
        {
            List<Vokabeln> vlist = new List<Vokabeln>();
            OleDbCommand cmd;
            int counter = 0;
            
            cmd = new OleDbCommand("SELECT de, en FROM vokabel WHERE kat = "+kat+";", Connection.GetConnection().DBConnection);

            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string d = reader.GetString(0);
                string e = reader.GetString(1);
                counter++;

                vlist.Add(new Vokabeln(d, e, 0));
            }
            reader.Close();


            Optionen.GetOption().numValue = counter;
            return vlist;
        }

        public int WriteToDB(int richtig, int falsch)
        {
            OleDbCommand cmd;

            cmd = new OleDbCommand("SELECT COUNT(id) FROM ergebnis",Connection.GetConnection().DBConnection);

            int id = (int)cmd.ExecuteScalar();
            /*
            DateTime myDateTime = DateTime.Now;
            var sqlFormattedDate = myDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss");
            */
            cmd = new OleDbCommand("INSERT INTO ergebnis VALUES("+(id+1)+",'" + DateTime.Now + "'," + richtig + "," + falsch + ");", Connection.GetConnection().DBConnection);

            int i = cmd.ExecuteNonQuery();

            return i;
        }


        public int AuswahlID { get; set; }
    }
}
