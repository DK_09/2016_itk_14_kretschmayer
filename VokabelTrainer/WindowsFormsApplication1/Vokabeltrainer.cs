﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;

namespace WindowsFormsApplication1
{
    public partial class Vokabeltrainer : Form
    {
        public Vokabeltrainer()
        {
            InitializeComponent();

            Connection.GetConnection().DBConnection.Open();

            Auswahl an = new Auswahl();

           // an.ShowDialog();

            DialogResult rs = an.ShowDialog();
            if (rs == DialogResult.OK)
            {
               // MessageBox.Show("k");
                int kat = Optionen.GetOption().AuswahlID;

                if (kat == 0)
                {
                    MessageBox.Show("Bitte nehmen Sie eine gültige Kategorie!");
                }
                else
                {
                    Optionen.GetOption().ListTempVok = Optionen.GetOption().GibVokabelList(kat);

                    Panel vocPan = Panelerzeugen();

                    CreateEntries(Optionen.GetOption().ListTempVok, vocPan, Optionen.GetOption().numValue);

                }
            }

            if (rs == DialogResult.Cancel)
            {
                //MessageBox.Show("Notok");
            }
        }

        internal Vokabeltrainer_Refactoring voktrain = new Vokabeltrainer_Refactoring();
       
        

        //Buttons
        private void check_Click(object sender, EventArgs e)
        {
            if (voktrain.Checken())
            {
                //CheckEntries
                CheckEntries();
            }
            else
            {
                MessageBox.Show("Bitte wählen Sie vorher eine Datei aus!");
            }
        }

        private void reload_Click(object sender, EventArgs e)
        {
            if (voktrain.Checken())
            {
                //DeletePanel
                DeletePanel(Optionen.GetOption().CurrentFilename);
            }
            else
            {
                MessageBox.Show("Bitte wählen Sie vorher eine Datei aus!");
            }
        }


        //Vokbale-Funktionen
        public void CheckEntries()
        {
            int richtig = 0;
            int falsch = 0;

            foreach (Control c in Controls)
            {
                if (c.Name == "vocPan")
                {
                    foreach (Control cc in c.Controls)
                    {
                        Vokabeln v = new Vokabeln("de", Convert.ToString(cc.Tag), 0);

                        if (cc is TextBox)
                        {
                            if (v.Check(cc.Text, v))
                            {
                                cc.BackColor = Color.Green;

                                richtig++;
                            }
                            else
                            {
                                cc.BackColor = Color.Red;

                                falsch++;
                            }
                        }
                    }

                    //Prozent rechnen
                    
                    MessageBox.Show(voktrain.ProzRechnen(richtig, falsch));

                    MessageBox.Show("" + Optionen.GetOption().WriteToDB(richtig, falsch));
                }
            }
        }

        internal void CreateEntries(List<Vokabeln> vokabellist, Panel vocPan, int anzahl)
        {
            int weite = 50;
            int höhe = 50;
            int zähler = 0;
            int anzahlzähler = 0;

            foreach (Vokabeln v in vokabellist)
            {
                if (Optionen.GetOption().mod == false)
                {
                    if (anzahlzähler < anzahl)
                    {

                        Label labl = new Label();
                        TextBox tbox = new TextBox();

                        labl.Text = v.De;
                        labl.Name = v.De;
                        labl.Tag = v.De;
                        labl.Top = höhe + zähler;
                        labl.Left = weite;

                        tbox.Name = v.En;
                        tbox.Tag = v.En;
                        tbox.Top = höhe + zähler;
                        tbox.Left = weite + 100;


                        zähler += 50;

                        vocPan.Controls.Add(labl);
                        vocPan.Controls.Add(tbox);


                        anzahlzähler++;
                    }

                    else
                        break;
                }

                else
                {
                    if (anzahlzähler < anzahl)
                    {

                        Label labl = new Label();
                        TextBox tbox = new TextBox();

                        labl.Text = v.En;
                        labl.Name = v.En;
                        labl.Tag = v.En;
                        labl.Top = höhe + zähler;
                        labl.Left = weite;

                        tbox.Name = v.De;
                        tbox.Tag = v.De;
                        tbox.Top = höhe + zähler;
                        tbox.Left = weite + 100;


                        zähler += 50;

                        vocPan.Controls.Add(labl);
                        vocPan.Controls.Add(tbox);


                        anzahlzähler++;
                    }

                    else
                        break;
                }

            }

            this.Controls.Add(vocPan);

            Optionen.GetOption().Panelhier = true;
        }

        internal List<Vokabeln> ReadFromFile(string file)
        {
            return voktrain.ReadFromFile(file);
        }

        internal List<Vokabeln> SortEntries(List<Vokabeln> vokabellist)
        {
            return voktrain.SortEntries(vokabellist);
        }

        public void DeletePanel(string filename)
        {
            Kontrolle();

            Panel vocPan = Panelerzeugen();

            Optionen.GetOption().ListTempVok = SortEntries(Optionen.GetOption().ListTempVok);

            CreateEntries(Optionen.GetOption().ListTempVok, vocPan, Optionen.GetOption().numValue);
        }




        //Menü
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
           
            
        }

        private void öffnenToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            //OpenFileDialog fileAuswahl = new OpenFileDialog();
            //string filename;

            ////fileAuswahl.FileName = ".voc";
            //fileAuswahl.Filter = "Vokabeldatei (*.voc)|*.voc";
            //fileAuswahl.ShowDialog();


            //if (fileAuswahl.FileName == null || fileAuswahl.FileName == "")
            //{

            //}

            //else
            //{
            //    filename = fileAuswahl.FileName;

            //    Optionen.GetOption().CurrentFilename = fileAuswahl.FileName;

            //    Kontrolle();

            //    Panel vocPan = Panelerzeugen();

            //    CreateEntries(ReadFromFile(filename), vocPan, Optionen.GetOption().numValue);
            //}

            //throw new NotImplementedException();

          //  öffnenToolStripMenuItem.Click += öffnenToolStripMenuItem_Click;
        }

        private void beendenToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
            //beendenToolStripMenuItem.Click += beendenToolStripMenuItem_Click;
        }

        private void einstellungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Einstellungen eForm = new Einstellungen();

            eForm.ShowDialog();
        }

        private void einstellungenToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            KategorieForm kForm = new KategorieForm();

            kForm.ShowDialog();
        }
        
        //Close
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Wollen Sie wirklich den Vokabeltrainer beenden?", "Beenden", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        //Variablen
        public void Kontrolle()
        {
            foreach (Control c in this.Controls)
            {
                if (c.Name == "vocPan")
                {
                    int indexwert = Controls.IndexOf(c);
                    Controls.RemoveAt(indexwert);
                }
            }
        }
        public Panel Panelerzeugen()
        {
            Panel vocPan = new Panel();

            vocPan.Name = "vocPan";
            vocPan.Left = 200;
            vocPan.Top = 100;
            vocPan.Height = 350;
            vocPan.Width = 500;
            vocPan.BackColor = Color.White;

            vocPan.AutoScroll = false;
            vocPan.HorizontalScroll.Enabled = false;
            vocPan.HorizontalScroll.Visible = false;
            vocPan.HorizontalScroll.Maximum = 0;
            vocPan.AutoScroll = true;

            return vocPan;
        }



        //Shortcut
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            EventArgs a = new EventArgs();

            if (e.Control && e.KeyCode.ToString() == "O")
            {
                öffnenToolStripMenuItem_Click_1(sender, a);
            }

            if (e.Control && e.KeyCode.ToString() == "P")
            {
                beendenToolStripMenuItem_Click_1(sender, a);
            }

            if (e.Control && e.KeyCode.ToString() == "I")
            {
                einstellungenToolStripMenuItem_Click(sender, a);
            }
        }


    }
}
