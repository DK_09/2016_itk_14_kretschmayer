﻿namespace WindowsFormsApplication1
{
    partial class Auswahl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_kat = new System.Windows.Forms.ListBox();
            this.button_ok = new System.Windows.Forms.Button();
            this.button_abbrechen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox_kat
            // 
            this.listBox_kat.FormattingEnabled = true;
            this.listBox_kat.Location = new System.Drawing.Point(29, 12);
            this.listBox_kat.Name = "listBox_kat";
            this.listBox_kat.Size = new System.Drawing.Size(239, 277);
            this.listBox_kat.TabIndex = 0;
            this.listBox_kat.SelectedIndexChanged += new System.EventHandler(this.listBox_kat_SelectedIndexChanged);
            // 
            // button_ok
            // 
            this.button_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_ok.Location = new System.Drawing.Point(12, 319);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(124, 45);
            this.button_ok.TabIndex = 1;
            this.button_ok.Text = "OK";
            this.button_ok.UseVisualStyleBackColor = true;
            // 
            // button_abbrechen
            // 
            this.button_abbrechen.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_abbrechen.Location = new System.Drawing.Point(165, 319);
            this.button_abbrechen.Name = "button_abbrechen";
            this.button_abbrechen.Size = new System.Drawing.Size(124, 45);
            this.button_abbrechen.TabIndex = 2;
            this.button_abbrechen.Text = "Abbrechen";
            this.button_abbrechen.UseVisualStyleBackColor = true;
            // 
            // Auswahl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 390);
            this.Controls.Add(this.button_abbrechen);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.listBox_kat);
            this.Name = "Auswahl";
            this.Text = "Auswahl";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_kat;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Button button_abbrechen;
    }
}