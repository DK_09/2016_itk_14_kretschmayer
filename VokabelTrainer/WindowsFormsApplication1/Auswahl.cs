﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace WindowsFormsApplication1
{
    public partial class Auswahl : Form
    {
        public Auswahl()
        {
            InitializeComponent();

            Aktualisieren();
        }

        public void Aktualisieren()
        {
            listBox_kat.Items.Clear();


            OleDbCommand cmd = new OleDbCommand("SELECT id, bereich FROM kategorie", Connection.GetConnection().DBConnection);

            List<Kategorie> klist = new List<Kategorie>();

            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                klist.Add(new Kategorie(reader.GetInt32(0), reader.GetString(1)));
            }

            reader.Close();


            foreach (Kategorie k in klist)
            {
                listBox_kat.Items.Add(k);
            }
        }

        private void listBox_kat_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Kategorie k = (Kategorie)listBox_kat.SelectedItem;

                Optionen.GetOption().AuswahlID = k.ID;
            }
            catch
            { 
            
            }
        }
    }
}
