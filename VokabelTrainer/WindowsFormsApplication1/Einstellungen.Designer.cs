﻿namespace WindowsFormsApplication1
{
    partial class Einstellungen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Einstellungen));
            this.speichern = new System.Windows.Forms.Button();
            this.abbrechen = new System.Windows.Forms.Button();
            this.modusBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.anzahlUpDown = new System.Windows.Forms.NumericUpDown();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.numeric1 = new System.Windows.Forms.NumericUpDown();
            this.numeric2 = new System.Windows.Forms.NumericUpDown();
            this.numeric3 = new System.Windows.Forms.NumericUpDown();
            this.numeric4 = new System.Windows.Forms.NumericUpDown();
            this.numeric5 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.anzahlUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric5)).BeginInit();
            this.SuspendLayout();
            // 
            // speichern
            // 
            this.speichern.Location = new System.Drawing.Point(136, 490);
            this.speichern.Name = "speichern";
            this.speichern.Size = new System.Drawing.Size(129, 42);
            this.speichern.TabIndex = 0;
            this.speichern.Text = "Speichern";
            this.speichern.UseVisualStyleBackColor = true;
            this.speichern.Click += new System.EventHandler(this.speichern_Click);
            // 
            // abbrechen
            // 
            this.abbrechen.Location = new System.Drawing.Point(300, 490);
            this.abbrechen.Name = "abbrechen";
            this.abbrechen.Size = new System.Drawing.Size(129, 42);
            this.abbrechen.TabIndex = 1;
            this.abbrechen.Text = "Abbrechen";
            this.abbrechen.UseVisualStyleBackColor = true;
            this.abbrechen.Click += new System.EventHandler(this.abbrechen_Click);
            // 
            // modusBox
            // 
            this.modusBox.FormattingEnabled = true;
            this.modusBox.Items.AddRange(new object[] {
            "DE-EN",
            "EN-DE"});
            this.modusBox.Location = new System.Drawing.Point(308, 166);
            this.modusBox.Name = "modusBox";
            this.modusBox.Size = new System.Drawing.Size(121, 21);
            this.modusBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(132, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Modus:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(132, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "Anzahl:";
            // 
            // anzahlUpDown
            // 
            this.anzahlUpDown.Location = new System.Drawing.Point(309, 72);
            this.anzahlUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.anzahlUpDown.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.anzahlUpDown.Name = "anzahlUpDown";
            this.anzahlUpDown.Size = new System.Drawing.Size(120, 20);
            this.anzahlUpDown.TabIndex = 5;
            this.anzahlUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(309, 259);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 6;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(309, 302);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 7;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(308, 341);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 8;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(308, 380);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 9;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(309, 422);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 10;
            // 
            // numeric1
            // 
            this.numeric1.Location = new System.Drawing.Point(140, 259);
            this.numeric1.Name = "numeric1";
            this.numeric1.Size = new System.Drawing.Size(120, 20);
            this.numeric1.TabIndex = 11;
            this.numeric1.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numeric2
            // 
            this.numeric2.Location = new System.Drawing.Point(140, 302);
            this.numeric2.Name = "numeric2";
            this.numeric2.Size = new System.Drawing.Size(120, 20);
            this.numeric2.TabIndex = 12;
            this.numeric2.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numeric3
            // 
            this.numeric3.Location = new System.Drawing.Point(140, 341);
            this.numeric3.Name = "numeric3";
            this.numeric3.Size = new System.Drawing.Size(120, 20);
            this.numeric3.TabIndex = 13;
            this.numeric3.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numeric4
            // 
            this.numeric4.Location = new System.Drawing.Point(140, 380);
            this.numeric4.Name = "numeric4";
            this.numeric4.Size = new System.Drawing.Size(120, 20);
            this.numeric4.TabIndex = 14;
            this.numeric4.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numeric5
            // 
            this.numeric5.Location = new System.Drawing.Point(140, 422);
            this.numeric5.Name = "numeric5";
            this.numeric5.Size = new System.Drawing.Size(120, 20);
            this.numeric5.TabIndex = 15;
            this.numeric5.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // Einstellungen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 559);
            this.Controls.Add(this.numeric5);
            this.Controls.Add(this.numeric4);
            this.Controls.Add(this.numeric3);
            this.Controls.Add(this.numeric2);
            this.Controls.Add(this.numeric1);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.anzahlUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.modusBox);
            this.Controls.Add(this.abbrechen);
            this.Controls.Add(this.speichern);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Einstellungen";
            this.Text = "Einstellungen";
            ((System.ComponentModel.ISupportInitialize)(this.anzahlUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button speichern;
        private System.Windows.Forms.Button abbrechen;
        private System.Windows.Forms.ComboBox modusBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown anzahlUpDown;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.NumericUpDown numeric1;
        private System.Windows.Forms.NumericUpDown numeric2;
        private System.Windows.Forms.NumericUpDown numeric3;
        private System.Windows.Forms.NumericUpDown numeric4;
        private System.Windows.Forms.NumericUpDown numeric5;
    }
}