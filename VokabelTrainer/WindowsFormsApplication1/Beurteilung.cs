﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Beurteilung
    {
        public int Prozent { get; set; }
        public string Beurteilungname { get; set; }

        public Beurteilung(int prozent, string bname)
        {
            this.Prozent = prozent;
            this.Beurteilungname = bname;
        }
    }
}
