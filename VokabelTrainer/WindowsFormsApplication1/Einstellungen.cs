﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Einstellungen : Form
    {
        public Einstellungen()
        {
            InitializeComponent();

            Optionen.GetOption().testanzahl = Convert.ToInt32(anzahlUpDown.Value);


            Optionen.GetOption().ReadOptions();

            //if (Optionen.GetOption().confbool == true)
            //{
            if (File.Exists("config.txt"))
            {
                if (Optionen.GetOption().numValue == 0)
                {

                }
                else
                    anzahlUpDown.Value = Optionen.GetOption().numValue;

                if (Optionen.GetOption().smod == null || Optionen.GetOption().smod == "")
                {

                }
                else
                    modusBox.Text = Optionen.GetOption().smod;

                


                //Dynamisch machen!!

                boxFüllen();
            }
            else
            {

            }
            //}
        }

        public int anzahl { get; set; }
            
        private void speichern_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Das wird erst in der Version 3.0 möglich sein!"); 
            Optionen.GetOption().isDirty = true;

            Optionen.GetOption().numValue = Convert.ToInt32(anzahlUpDown.Value);

            if (Convert.ToString(modusBox.SelectedItem) == "EN-DE")
            {
                Optionen.GetOption().mod = true;
            }
            else
                Optionen.GetOption().mod = false;

            List<Beurteilung> blist = new List<Beurteilung>();

            blist.Add(new Beurteilung(Convert.ToInt32(numeric1.Value), Convert.ToString(textBox1.Text)));
            blist.Add(new Beurteilung(Convert.ToInt32(numeric2.Value), textBox2.Text));
            blist.Add(new Beurteilung(Convert.ToInt32(numeric3.Value), textBox3.Text));
            blist.Add(new Beurteilung(Convert.ToInt32(numeric4.Value), textBox4.Text));
            blist.Add(new Beurteilung(Convert.ToInt32(numeric5.Value), textBox5.Text));


            Optionen.GetOption().blist = blist;


            
            //Optionen.GetOption().ReadOptions();
            Optionen.GetOption().WriteOptions(Optionen.GetOption().isDirty);

            //Optionen.GetOption().confbool = true;
            //ReadOptions();
            //WriteOptions(isDirty);



            this.Close();
        }

        private void abbrechen_Click(object sender, EventArgs e)
        {
           this.Close();
        }


        private void boxFüllen()
        {
            for (int i = 0; i < Optionen.GetOption().blist.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        numeric1.Value = Optionen.GetOption().blist[0].Prozent;
                        textBox1.Text = Optionen.GetOption().blist[0].Beurteilungname;
                        break;

                    case 1:
                        numeric2.Value = Optionen.GetOption().blist[1].Prozent;
                        textBox2.Text = Optionen.GetOption().blist[1].Beurteilungname;
                        break;

                    case 2:
                        numeric3.Value = Optionen.GetOption().blist[2].Prozent;
                        textBox3.Text = Optionen.GetOption().blist[2].Beurteilungname;
                        break;

                    case 3:
                        numeric4.Value = Optionen.GetOption().blist[3].Prozent;
                        textBox4.Text = Optionen.GetOption().blist[3].Beurteilungname;
                        break;

                    case 4:
                        numeric5.Value = Optionen.GetOption().blist[4].Prozent;
                        textBox5.Text = Optionen.GetOption().blist[4].Beurteilungname;
                        break;
                }
            }
        }

    }
}
