﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace WindowsFormsApplication1
{
    class Connection
    {
        public bool isDirty;

        public OleDbConnection DBConnection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=VokabelDB.mdb");

        private static Connection con;

        private Connection()
        {
            
        }

        public static Connection GetConnection()
        {
            if (con == null)
            {
                con = new Connection();
            }

            return con;
        }
    }
}
