﻿namespace WindowsFormsApplication1
{
    partial class KategorieForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_erstellen = new System.Windows.Forms.Button();
            this.button_ändern = new System.Windows.Forms.Button();
            this.button_löschen = new System.Windows.Forms.Button();
            this.label_id = new System.Windows.Forms.Label();
            this.label_bereich = new System.Windows.Forms.Label();
            this.textBox_ID = new System.Windows.Forms.TextBox();
            this.richTextBox_Beschreibung = new System.Windows.Forms.RichTextBox();
            this.button_speichern = new System.Windows.Forms.Button();
            this.listBox_Kategorien = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // button_erstellen
            // 
            this.button_erstellen.Location = new System.Drawing.Point(366, 214);
            this.button_erstellen.Name = "button_erstellen";
            this.button_erstellen.Size = new System.Drawing.Size(138, 46);
            this.button_erstellen.TabIndex = 0;
            this.button_erstellen.Text = "Erstellen";
            this.button_erstellen.UseVisualStyleBackColor = true;
            this.button_erstellen.Click += new System.EventHandler(this.button_erstellen_Click);
            // 
            // button_ändern
            // 
            this.button_ändern.Location = new System.Drawing.Point(366, 279);
            this.button_ändern.Name = "button_ändern";
            this.button_ändern.Size = new System.Drawing.Size(138, 46);
            this.button_ändern.TabIndex = 1;
            this.button_ändern.Text = "Ändern";
            this.button_ändern.UseVisualStyleBackColor = true;
            this.button_ändern.Click += new System.EventHandler(this.button_ändern_Click);
            // 
            // button_löschen
            // 
            this.button_löschen.Location = new System.Drawing.Point(570, 214);
            this.button_löschen.Name = "button_löschen";
            this.button_löschen.Size = new System.Drawing.Size(138, 46);
            this.button_löschen.TabIndex = 2;
            this.button_löschen.Text = "Löschen";
            this.button_löschen.UseVisualStyleBackColor = true;
            this.button_löschen.Click += new System.EventHandler(this.button_löschen_Click);
            // 
            // label_id
            // 
            this.label_id.AutoSize = true;
            this.label_id.Location = new System.Drawing.Point(329, 27);
            this.label_id.Name = "label_id";
            this.label_id.Size = new System.Drawing.Size(21, 13);
            this.label_id.TabIndex = 3;
            this.label_id.Text = "ID:";
            // 
            // label_bereich
            // 
            this.label_bereich.AutoSize = true;
            this.label_bereich.Location = new System.Drawing.Point(329, 78);
            this.label_bereich.Name = "label_bereich";
            this.label_bereich.Size = new System.Drawing.Size(46, 13);
            this.label_bereich.TabIndex = 4;
            this.label_bereich.Text = "Bereich:";
            // 
            // textBox_ID
            // 
            this.textBox_ID.Location = new System.Drawing.Point(400, 24);
            this.textBox_ID.Name = "textBox_ID";
            this.textBox_ID.ReadOnly = true;
            this.textBox_ID.Size = new System.Drawing.Size(156, 20);
            this.textBox_ID.TabIndex = 5;
            // 
            // richTextBox_Beschreibung
            // 
            this.richTextBox_Beschreibung.Location = new System.Drawing.Point(400, 75);
            this.richTextBox_Beschreibung.Name = "richTextBox_Beschreibung";
            this.richTextBox_Beschreibung.ReadOnly = true;
            this.richTextBox_Beschreibung.Size = new System.Drawing.Size(273, 117);
            this.richTextBox_Beschreibung.TabIndex = 6;
            this.richTextBox_Beschreibung.Text = "";
            // 
            // button_speichern
            // 
            this.button_speichern.Location = new System.Drawing.Point(570, 279);
            this.button_speichern.Name = "button_speichern";
            this.button_speichern.Size = new System.Drawing.Size(138, 46);
            this.button_speichern.TabIndex = 7;
            this.button_speichern.Text = "Speichern";
            this.button_speichern.UseVisualStyleBackColor = true;
            this.button_speichern.Click += new System.EventHandler(this.button_speichern_Click);
            // 
            // listBox_Kategorien
            // 
            this.listBox_Kategorien.FormattingEnabled = true;
            this.listBox_Kategorien.Location = new System.Drawing.Point(38, 36);
            this.listBox_Kategorien.Name = "listBox_Kategorien";
            this.listBox_Kategorien.Size = new System.Drawing.Size(227, 277);
            this.listBox_Kategorien.TabIndex = 8;
            this.listBox_Kategorien.SelectedIndexChanged += new System.EventHandler(this.listBox_Kategorien_SelectedIndexChanged);
            // 
            // KategorieForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 385);
            this.Controls.Add(this.listBox_Kategorien);
            this.Controls.Add(this.button_speichern);
            this.Controls.Add(this.richTextBox_Beschreibung);
            this.Controls.Add(this.textBox_ID);
            this.Controls.Add(this.label_bereich);
            this.Controls.Add(this.label_id);
            this.Controls.Add(this.button_löschen);
            this.Controls.Add(this.button_ändern);
            this.Controls.Add(this.button_erstellen);
            this.Name = "KategorieForm";
            this.Text = "Kategorie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_erstellen;
        private System.Windows.Forms.Button button_ändern;
        private System.Windows.Forms.Button button_löschen;
        private System.Windows.Forms.Label label_id;
        private System.Windows.Forms.Label label_bereich;
        private System.Windows.Forms.TextBox textBox_ID;
        private System.Windows.Forms.RichTextBox richTextBox_Beschreibung;
        private System.Windows.Forms.Button button_speichern;
        private System.Windows.Forms.ListBox listBox_Kategorien;
    }
}