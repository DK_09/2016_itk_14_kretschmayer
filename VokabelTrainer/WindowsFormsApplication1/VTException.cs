﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class VTException : ApplicationException
    {
        public VTException(string message):
            base(message)
        {

        }

        public VTException(string message, Exception innerException):
            base(message, innerException)
        { 
        
        }

        public VTException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { 
            
        }
    }
}
