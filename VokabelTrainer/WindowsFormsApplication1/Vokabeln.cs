﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Vokabeln
    {
        private string de;
        private string en;
        private int nummer;


        public Vokabeln(string de, string en, int nummer)
        {
            this.de = de;
            this.en = en;
            this.nummer = nummer;
        }

        public string De
        {
            get { return de; }
            set { de = value; }
        }
        public string En
        {
            get { return en;}
            set { en = value; }
        }
        public int Nummer
        {
            get { return nummer; }
            set { nummer = value; }
        }


        public bool Check(string text, Vokabeln v)
        {
            if (v.En.ToUpper() == text.ToUpper())
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}
