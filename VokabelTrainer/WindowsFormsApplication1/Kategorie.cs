﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Kategorie
    {
        public int ID { get; set; }

        public string Bereich { get; set; }

        public bool isDirty;

        public Kategorie(int id, string bereich)
        {
            this.ID = id;
            this.Bereich = bereich;
        }

        public override string ToString()
        {

            return this.Bereich;
            //return base.ToString();
        }
    }
}
