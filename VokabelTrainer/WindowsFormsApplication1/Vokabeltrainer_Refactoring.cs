﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    class Vokabeltrainer_Refactoring
    {
        public Vokabeltrainer_Refactoring()
        {

        }

        //Buttons

        public bool Checken()
        {
            //if (Optionen.GetOption().CurrentFilename != null)
            //{
            //    return true;
            //}
            //else
            //    return false;

            if (Optionen.GetOption().Panelhier)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //Vokabel-Funktionen
        public string ProzRechnen(int richtig, int falsch)
        {
            int gesamt = 0;
            double prozRichtig = 0;
            string bname = "";

            List<Beurteilung> blist = new List<Beurteilung>();


            gesamt = richtig + falsch;
            //zB.: gesamt = 4, richtig = 1, falsch = 3,                 100%, 25%, 75%
            //100/4=25 = ein viertl; * richtig = sind die anteile die richtig sind in prozent

            if (gesamt == 0)
            {
                MessageBox.Show("Die Anzahl der Vokabeln beträgt 0, daher konnte nichts überprüft werden!");
            }
            else
            {
                prozRichtig = (100 / gesamt) * richtig;
            
                blist = Optionen.GetOption().blist;

                blist.Sort(
                    delegate(Beurteilung b1, Beurteilung b2)
                    {
                        return b1.Prozent.CompareTo(b2.Prozent);
                    }
                    );

                for (int i = 0; i < blist.Count; i++)
                {
                    if (prozRichtig <= blist[i].Prozent)
                    {
                        if (prozRichtig == blist[i].Prozent)
                        {
                            //MessageBox.Show(blist[i].Beurteilungname);

                            bname = blist[i].Beurteilungname;
                            return bname;
                        }

                        else
                        {
                            if (i == 0)
                            {
                                //MessageBox.Show(blist[i].Beurteilungname);

                                bname = blist[i].Beurteilungname;
                                return bname;
                            }
                            else
                            {
                                //MessageBox.Show(blist[i - 1].Beurteilungname);

                                bname = blist[i - 1].Beurteilungname;
                                return bname;
                            }
                        }
                    }
                }

            }
            return bname;
        }

        public List<Vokabeln> ReadFromFile(string file)
        {
            List<Vokabeln> vokabellist = new List<Vokabeln>();
            StreamReader sr = new StreamReader(file);

            while (sr.Peek() != -1)
            {
                try
                {
                    string zeile = sr.ReadLine();
                    string[] zarr = zeile.Split(';');

                    vokabellist.Add(new Vokabeln(zarr[0], zarr[1], 0));
                }
                catch(Exception ex)
                {
                    VTException vte = new VTException("Feeeeeeeeehler!!!!1", ex);
                    MessageBox.Show(DateTime.Now + "\t" + vte.InnerException);


                    StreamWriter sw = new StreamWriter("vt_errors.log", true);

                    sw.WriteLine(DateTime.Now + "\t" + vte.InnerException + "\n");

                    sw.Flush();
                    sw.Close();

                    break;
                }
            }
            sr.Close();


            return vokabellist;
        }

        public List<Vokabeln> SortEntries(List<Vokabeln> vokabellist)
        {
            Random rand = new Random();
            int anzahl = vokabellist.Count();

            foreach (Vokabeln v in vokabellist)
            {
                int nummer = rand.Next(0, anzahl);

                v.Nummer = nummer;
            }

            vokabellist.Sort(delegate(Vokabeln a, Vokabeln b)
            {
                return a.Nummer.CompareTo(b.Nummer);
            }
                );

            return vokabellist;
        }

    }
}