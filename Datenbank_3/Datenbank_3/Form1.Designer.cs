﻿namespace Datenbank_3
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_artikelverwaltung = new System.Windows.Forms.Label();
            this.listBox_artikel = new System.Windows.Forms.ListBox();
            this.label_ean = new System.Windows.Forms.Label();
            this.label_bezeichnung = new System.Windows.Forms.Label();
            this.label_verkaufspreis = new System.Windows.Forms.Label();
            this.label_sortiment = new System.Windows.Forms.Label();
            this.label_lagerbestand = new System.Windows.Forms.Label();
            this.button_neu = new System.Windows.Forms.Button();
            this.button_speichern = new System.Windows.Forms.Button();
            this.button_löschen = new System.Windows.Forms.Button();
            this.textBox_ean = new System.Windows.Forms.TextBox();
            this.textBox_verkaufspreis = new System.Windows.Forms.TextBox();
            this.textBox_sortiment = new System.Windows.Forms.TextBox();
            this.textBox_lagerbestand = new System.Windows.Forms.TextBox();
            this.richTextBox_bezeichnung = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label_artikelverwaltung
            // 
            this.label_artikelverwaltung.AutoSize = true;
            this.label_artikelverwaltung.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_artikelverwaltung.Location = new System.Drawing.Point(39, 44);
            this.label_artikelverwaltung.Name = "label_artikelverwaltung";
            this.label_artikelverwaltung.Size = new System.Drawing.Size(152, 24);
            this.label_artikelverwaltung.TabIndex = 0;
            this.label_artikelverwaltung.Text = "Artikelverwaltung";
            // 
            // listBox_artikel
            // 
            this.listBox_artikel.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox_artikel.FormattingEnabled = true;
            this.listBox_artikel.ItemHeight = 22;
            this.listBox_artikel.Location = new System.Drawing.Point(43, 82);
            this.listBox_artikel.Name = "listBox_artikel";
            this.listBox_artikel.Size = new System.Drawing.Size(297, 378);
            this.listBox_artikel.TabIndex = 1;
            this.listBox_artikel.SelectedIndexChanged += new System.EventHandler(this.listBox_artikel_SelectedIndexChanged);
            // 
            // label_ean
            // 
            this.label_ean.AutoSize = true;
            this.label_ean.Location = new System.Drawing.Point(421, 112);
            this.label_ean.Name = "label_ean";
            this.label_ean.Size = new System.Drawing.Size(32, 13);
            this.label_ean.TabIndex = 2;
            this.label_ean.Text = "EAN:";
            // 
            // label_bezeichnung
            // 
            this.label_bezeichnung.AutoSize = true;
            this.label_bezeichnung.Location = new System.Drawing.Point(421, 149);
            this.label_bezeichnung.Name = "label_bezeichnung";
            this.label_bezeichnung.Size = new System.Drawing.Size(69, 13);
            this.label_bezeichnung.TabIndex = 3;
            this.label_bezeichnung.Text = "Bezeichnung";
            // 
            // label_verkaufspreis
            // 
            this.label_verkaufspreis.AutoSize = true;
            this.label_verkaufspreis.Location = new System.Drawing.Point(421, 283);
            this.label_verkaufspreis.Name = "label_verkaufspreis";
            this.label_verkaufspreis.Size = new System.Drawing.Size(74, 13);
            this.label_verkaufspreis.TabIndex = 4;
            this.label_verkaufspreis.Text = "Verkaufspreis:";
            // 
            // label_sortiment
            // 
            this.label_sortiment.AutoSize = true;
            this.label_sortiment.Location = new System.Drawing.Point(421, 322);
            this.label_sortiment.Name = "label_sortiment";
            this.label_sortiment.Size = new System.Drawing.Size(86, 13);
            this.label_sortiment.TabIndex = 5;
            this.label_sortiment.Text = "Im Sortiment ab: ";
            // 
            // label_lagerbestand
            // 
            this.label_lagerbestand.AutoSize = true;
            this.label_lagerbestand.Location = new System.Drawing.Point(421, 363);
            this.label_lagerbestand.Name = "label_lagerbestand";
            this.label_lagerbestand.Size = new System.Drawing.Size(72, 13);
            this.label_lagerbestand.TabIndex = 6;
            this.label_lagerbestand.Text = "Lagerbestand";
            // 
            // button_neu
            // 
            this.button_neu.Location = new System.Drawing.Point(390, 437);
            this.button_neu.Name = "button_neu";
            this.button_neu.Size = new System.Drawing.Size(117, 40);
            this.button_neu.TabIndex = 7;
            this.button_neu.Text = "Neuanlage";
            this.button_neu.UseVisualStyleBackColor = true;
            this.button_neu.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_speichern
            // 
            this.button_speichern.Location = new System.Drawing.Point(557, 437);
            this.button_speichern.Name = "button_speichern";
            this.button_speichern.Size = new System.Drawing.Size(117, 40);
            this.button_speichern.TabIndex = 8;
            this.button_speichern.Text = "Speichern";
            this.button_speichern.UseVisualStyleBackColor = true;
            this.button_speichern.Click += new System.EventHandler(this.button_speichern_Click);
            // 
            // button_löschen
            // 
            this.button_löschen.Location = new System.Drawing.Point(716, 437);
            this.button_löschen.Name = "button_löschen";
            this.button_löschen.Size = new System.Drawing.Size(117, 40);
            this.button_löschen.TabIndex = 9;
            this.button_löschen.Text = "Löschen";
            this.button_löschen.UseVisualStyleBackColor = true;
            this.button_löschen.Click += new System.EventHandler(this.button_löschen_Click);
            // 
            // textBox_ean
            // 
            this.textBox_ean.Location = new System.Drawing.Point(532, 109);
            this.textBox_ean.Name = "textBox_ean";
            this.textBox_ean.ReadOnly = true;
            this.textBox_ean.Size = new System.Drawing.Size(171, 20);
            this.textBox_ean.TabIndex = 10;
            // 
            // textBox_verkaufspreis
            // 
            this.textBox_verkaufspreis.Location = new System.Drawing.Point(532, 280);
            this.textBox_verkaufspreis.Name = "textBox_verkaufspreis";
            this.textBox_verkaufspreis.ReadOnly = true;
            this.textBox_verkaufspreis.Size = new System.Drawing.Size(142, 20);
            this.textBox_verkaufspreis.TabIndex = 12;
            // 
            // textBox_sortiment
            // 
            this.textBox_sortiment.Location = new System.Drawing.Point(532, 319);
            this.textBox_sortiment.Name = "textBox_sortiment";
            this.textBox_sortiment.ReadOnly = true;
            this.textBox_sortiment.Size = new System.Drawing.Size(142, 20);
            this.textBox_sortiment.TabIndex = 13;
            // 
            // textBox_lagerbestand
            // 
            this.textBox_lagerbestand.Location = new System.Drawing.Point(532, 360);
            this.textBox_lagerbestand.Name = "textBox_lagerbestand";
            this.textBox_lagerbestand.ReadOnly = true;
            this.textBox_lagerbestand.Size = new System.Drawing.Size(100, 20);
            this.textBox_lagerbestand.TabIndex = 14;
            // 
            // richTextBox_bezeichnung
            // 
            this.richTextBox_bezeichnung.Location = new System.Drawing.Point(532, 149);
            this.richTextBox_bezeichnung.Name = "richTextBox_bezeichnung";
            this.richTextBox_bezeichnung.ReadOnly = true;
            this.richTextBox_bezeichnung.Size = new System.Drawing.Size(212, 96);
            this.richTextBox_bezeichnung.TabIndex = 15;
            this.richTextBox_bezeichnung.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 570);
            this.Controls.Add(this.richTextBox_bezeichnung);
            this.Controls.Add(this.textBox_lagerbestand);
            this.Controls.Add(this.textBox_sortiment);
            this.Controls.Add(this.textBox_verkaufspreis);
            this.Controls.Add(this.textBox_ean);
            this.Controls.Add(this.button_löschen);
            this.Controls.Add(this.button_speichern);
            this.Controls.Add(this.button_neu);
            this.Controls.Add(this.label_lagerbestand);
            this.Controls.Add(this.label_sortiment);
            this.Controls.Add(this.label_verkaufspreis);
            this.Controls.Add(this.label_bezeichnung);
            this.Controls.Add(this.label_ean);
            this.Controls.Add(this.listBox_artikel);
            this.Controls.Add(this.label_artikelverwaltung);
            this.Name = "Form1";
            this.Text = "Ersatzteillager Wien";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_artikelverwaltung;
        private System.Windows.Forms.ListBox listBox_artikel;
        private System.Windows.Forms.Label label_ean;
        private System.Windows.Forms.Label label_bezeichnung;
        private System.Windows.Forms.Label label_verkaufspreis;
        private System.Windows.Forms.Label label_sortiment;
        private System.Windows.Forms.Label label_lagerbestand;
        private System.Windows.Forms.Button button_neu;
        private System.Windows.Forms.Button button_speichern;
        private System.Windows.Forms.Button button_löschen;
        private System.Windows.Forms.TextBox textBox_ean;
        private System.Windows.Forms.TextBox textBox_verkaufspreis;
        private System.Windows.Forms.TextBox textBox_sortiment;
        private System.Windows.Forms.TextBox textBox_lagerbestand;
        private System.Windows.Forms.RichTextBox richTextBox_bezeichnung;
    }
}

