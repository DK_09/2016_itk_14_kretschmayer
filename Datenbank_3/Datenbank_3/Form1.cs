﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Datenbank_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            try
            {
                Connection.GetConnection().DBConnection.Open();

                OleDbCommand cmd = new OleDbCommand("SELECT id, bezeichnung from artikel", Connection.GetConnection().DBConnection);

                OleDbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Connection.GetConnection().list_art.Add(new Artikel((int)reader["id"],(string)reader["bezeichnung"]));
                    //listBox_artikel.Items.Add(reader[0]);
                }
                reader.Close();

                foreach (Artikel a in Connection.GetConnection().list_art)
                {
                    listBox_artikel.Items.Add(a);
                }

               
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Connection.GetConnection().DBConnection.Close();
        }

        private void listBox_artikel_SelectedIndexChanged(object sender, EventArgs e)
        {
            Artikel aktuell = (Artikel)listBox_artikel.SelectedItem;

            OleDbCommand cmd = new OleDbCommand("SELECT ean, bezeichnung, lager, imsortiment, vkp from artikel WHERE id = "+aktuell.ID+" ;", Connection.GetConnection().DBConnection);

            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                textBox_ean.Text = Convert.ToString(reader["ean"]);
                richTextBox_bezeichnung.Text = (string)reader["bezeichnung"];
                textBox_lagerbestand.Text = Convert.ToString((int)reader["lager"]);
                textBox_sortiment.Text = Convert.ToString((DateTime) reader["imsortiment"]);
                textBox_verkaufspreis.Text = Convert.ToString((double)reader["vkp"]);

                richTextBox_bezeichnung.ReadOnly = false;
                textBox_verkaufspreis.ReadOnly = false;
                textBox_lagerbestand.ReadOnly = false;
                textBox_ean.ReadOnly = true;
                textBox_sortiment.ReadOnly = true;
            }

            reader.Close();
        }

        private void button1_Click(object sender, EventArgs e)              //Neuanlage
        {
            richTextBox_bezeichnung.ReadOnly = false;
            textBox_verkaufspreis.ReadOnly = false;
            textBox_lagerbestand.ReadOnly = false;
            textBox_ean.ReadOnly = false;
            textBox_sortiment.ReadOnly = false;

            richTextBox_bezeichnung.Clear();
            textBox_verkaufspreis.Clear();
            textBox_lagerbestand.Clear();
            textBox_ean.Clear();
            textBox_sortiment.Clear();

            Connection.GetConnection().Neu = true;
        }

        private void button_speichern_Click(object sender, EventArgs e)
        {
            Artikel aktuell = (Artikel)listBox_artikel.SelectedItem;
            OleDbCommand cmd;
            OleDbDataReader reader;
            int listenanzahl = Connection.GetConnection().list_art.Count;

            if (Connection.GetConnection().Neu == true)
            {
                int en = Convert.ToInt32(textBox_ean.Text);
                string bz = richTextBox_bezeichnung.Text;
                int l = Convert.ToInt32(textBox_lagerbestand.Text);
                DateTime d = Convert.ToDateTime(textBox_sortiment.Text);
                double v = Convert.ToDouble(textBox_verkaufspreis.Text);

                //cmd = new OleDbCommand("INSERT INTO artikel VALUES (" + (listenanzahl + 1) + "," + en + ",'" + bz + "'," + l + ",'" + d + "'," + v + ",0,);", Connection.GetConnection().DBConnection);
                //cmd = new OleDbCommand("INSERT INTO artikel VALUES (6,2134,'adsf',12,'2000-02-12', 20.3, 0)",Connection.GetConnection().DBConnection);
                  cmd = new OleDbCommand("INSERT INTO artikel VALUES (" + (listenanzahl + 1) + "," + en + ",'" + bz + "'," + l + ",'" + d + "', " + v + ", 0)", Connection.GetConnection().DBConnection);
                   
                int row = (int)cmd.ExecuteNonQuery();

                MessageBox.Show("Zeilen geändert: "+row);
            }
            else
            {

                cmd = new OleDbCommand("SELECT bezeichnung, vkp, lager FROM artikel WHERE id = " + aktuell.ID + ";", Connection.GetConnection().DBConnection);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string bez = (string)reader["bezeichnung"];
                    double vkp = (double)reader["vkp"];
                    int lager = (int)reader["lager"];

                    if (bez != richTextBox_bezeichnung.Text || vkp != Convert.ToDouble(textBox_verkaufspreis.Text) || lager != Convert.ToInt32(textBox_lagerbestand.Text))
                    {
                        aktuell.isDirty = true;
                    }
                    else
                    {
                        aktuell.isDirty = false;
                    }
                }


                reader.Close();




                if (aktuell.isDirty == true)
                {
                    cmd = new OleDbCommand("UPDATE artikel SET bezeichnung = '" + richTextBox_bezeichnung.Text + "', vkp = '" + Convert.ToDouble(textBox_verkaufspreis.Text) + "', lager = '" + Convert.ToInt32(textBox_lagerbestand.Text) + "' WHERE id = " + aktuell.ID + ";", Connection.GetConnection().DBConnection);

                    int row = cmd.ExecuteNonQuery();

                    MessageBox.Show("Zeilen geändert: " + row);
                }
                else
                {

                }

            }


            Aktualisieren(cmd);


        }

            
        private void button_löschen_Click(object sender, EventArgs e)
        {
            Artikel aktuell = (Artikel)listBox_artikel.SelectedItem;
            OleDbCommand cmd;

            cmd = new OleDbCommand("DELETE FROM artikel WHERE id = "+aktuell.ID+";", Connection.GetConnection().DBConnection);

            int row = cmd.ExecuteNonQuery();

            MessageBox.Show(row+" Artikel wurde gelöscht!");


            Aktualisieren(cmd);
        }

        public void Aktualisieren(OleDbCommand cmd)
        {
            OleDbDataReader reader;

            listBox_artikel.Items.Clear();
            Connection.GetConnection().list_art.Clear();


            cmd = new OleDbCommand("SELECT id, bezeichnung from artikel", Connection.GetConnection().DBConnection);

            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Connection.GetConnection().list_art.Add(new Artikel((int)reader["id"], (string)reader["bezeichnung"]));
                //listBox_artikel.Items.Add(reader[0]);
            }
            reader.Close();


            foreach (Artikel a in Connection.GetConnection().list_art)
            {
                listBox_artikel.Items.Add(a);
            }


            richTextBox_bezeichnung.Clear();
            textBox_verkaufspreis.Clear();
            textBox_lagerbestand.Clear();
            textBox_ean.Clear();
            textBox_sortiment.Clear();

            Connection.GetConnection().Neu = false;
        }




        private void textBox_bezeichnung_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
