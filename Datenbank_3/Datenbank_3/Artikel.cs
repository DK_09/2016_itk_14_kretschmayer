﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace Datenbank_3
{
    class Artikel
    {
        public int ID { get; set; }
        public int EAN { get; set; }
        public string Bezeichnung { get; set; }
        public double Verkaufspreis { get; set; }
        public DateTime ImSortiment { get; set; }
        public int Lagerbestand { get; set; }
        public bool isDirty { get; set; }

        public Artikel()
        { 
        
        }

        public Artikel(int id)
        {
            this.ID = id;
        }

        public Artikel(int id, string bezeichnung)
        {
            this.ID = id;
            this.Bezeichnung = bezeichnung;
        }

        public Artikel(int id, int ean, string bezeichnung, int lagerbestand, DateTime imsortiment, double verkaufspreis)
        {
            this.ID = id;
            this.EAN = ean;
            this.Bezeichnung = bezeichnung;
            this.Lagerbestand = lagerbestand;
            this.ImSortiment = imsortiment;
            this.Verkaufspreis = verkaufspreis;
        }

        public void writeToDB()
        {
            OleDbCommand cmd = new OleDbCommand("UPDATE", Connection.GetConnection().DBConnection);
        }

        public void deleteFromDB()
        {
            OleDbCommand cmd = new OleDbCommand("DELETE", Connection.GetConnection().DBConnection);
        }

        public override string ToString()
        {
            string s = this.Bezeichnung;

            if (this.Bezeichnung.Length >= 20)
            {
                s = s.Substring(0,25);
                s = s + "...";
            }

            return s;           
            //return base.ToString();
        }
    }
}
