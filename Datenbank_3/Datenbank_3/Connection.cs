﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace Datenbank_3
{
    class Connection
    {
        public OleDbConnection DBConnection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=artikelDB.mdb");

        public List<Artikel> list_art = new List<Artikel>();
        public int CurrentID { get; set; }
        public bool Neu { get; set; }

        private static Connection con;

        private Connection()
        {
            
        }

        public static Connection GetConnection()
        {
            if (con == null)
            {
                con = new Connection();
            }

            return con;
        }
    }
}
