﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDelegateLift
{
    class Lift
    {
        public int MaxKg { get; set; }

        private List<Person> pli = new List<Person>();

        public Lift(int max)
        {
            MaxKg = max;
        }

        public void Zusteigen(Person p)
        {
            pli.Add(p);
            pli.Sort(delegate(Person p1, Person p2)
                {
                   return p1.Gewicht.CompareTo(p2.Gewicht);
                });

            //Vergleichen des Gewichts
        }
    }
}
