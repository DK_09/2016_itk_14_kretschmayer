﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Vokabeltrainer : Form
    {
        public Vokabeltrainer()
        {
            InitializeComponent();
        }

        //Buttons
        private void check_Click(object sender, EventArgs e)
        {
            if (this.CurrentFileName != null)
            {
                CheckEntries();
            }
            else
            {
                MessageBox.Show("Bitte wählen Sie vorher eine Datei aus!");
            }
        }

        private void reload_Click(object sender, EventArgs e)
        {
            if (this.CurrentFileName != null)
            {
                DeletePanel(this.CurrentFileName);
            }
            else
            {
                MessageBox.Show("Bitte wählen Sie vorher eine Datei aus!");
            }
        }


        //Vokbale-Funktionen
        internal void CheckEntries()
        {
            foreach (Control c in Controls)
            {
                if (c.Name == "vocPan")
                {
                    foreach (Control cc in c.Controls)
                    {
                        Vokabeln v = new Vokabeln("de", Convert.ToString(cc.Tag), 0);

                        if (cc is TextBox)
                        {
                            if (v.En.ToUpper() == cc.Text.ToUpper())
                            {
                                cc.BackColor = Color.Green;
                            }

                            else
                            {
                                cc.BackColor = Color.Red;
                            }
                        }
                    }
                }
            }
        }

        internal void CreateEntries(List<Vokabeln> vokabellist, Panel vocPan, int anzahl)
        {
            int weite = 50;
            int höhe = 50;
            int zähler = 0;
            int anzahlzähler = 0;

            foreach (Vokabeln v in vokabellist)
            {
                if (Optionen.GetOption().mod == false)
                {
                    if (anzahlzähler < anzahl)
                    {

                        Label labl = new Label();
                        TextBox tbox = new TextBox();

                        labl.Text = v.De;
                        labl.Name = v.De;
                        labl.Tag = v.De;
                        labl.Top = höhe + zähler;
                        labl.Left = weite;

                        tbox.Name = v.En;
                        tbox.Tag = v.En;
                        tbox.Top = höhe + zähler;
                        tbox.Left = weite + 100;


                        zähler += 50;

                        vocPan.Controls.Add(labl);
                        vocPan.Controls.Add(tbox);


                        anzahlzähler++;
                    }

                    else
                        break;
                }

                else
                {
                    if (anzahlzähler < anzahl)
                    {

                        Label labl = new Label();
                        TextBox tbox = new TextBox();

                        labl.Text = v.En;
                        labl.Name = v.En;
                        labl.Tag = v.En;
                        labl.Top = höhe + zähler;
                        labl.Left = weite;

                        tbox.Name = v.De;
                        tbox.Tag = v.De;
                        tbox.Top = höhe + zähler;
                        tbox.Left = weite + 100;


                        zähler += 50;

                        vocPan.Controls.Add(labl);
                        vocPan.Controls.Add(tbox);


                        anzahlzähler++;
                    }

                    else
                        break;
                }

            }

            this.Controls.Add(vocPan);
        }

        internal List<Vokabeln> ReadFromFile(string file)
        {
            List<Vokabeln> vokabellist = new List<Vokabeln>();
            StreamReader sr = new StreamReader(file);

            while (sr.Peek() != -1)
            {
                string zeile = sr.ReadLine();
                string[] zarr = zeile.Split(';');

                vokabellist.Add(new Vokabeln(zarr[0], zarr[1], 0));
            }
            sr.Close();


            return vokabellist;
        }

        internal List<Vokabeln> SortEntries(List<Vokabeln> vokabellist)
        {
            Random rand = new Random();
            int anzahl = vokabellist.Count();

            foreach (Vokabeln v in vokabellist)
            {
                int nummer = rand.Next(0, anzahl);

                v.Nummer = nummer;
            }

            vokabellist.Sort(delegate(Vokabeln a, Vokabeln b)
            {
                return a.Nummer.CompareTo(b.Nummer);
            }
                );

            return vokabellist;
        }

        internal void DeletePanel(string filename)
        {
            Kontrolle();

            Panel vocPan = Panelerzeugen();

            CreateEntries(SortEntries(ReadFromFile(filename)), vocPan, Optionen.GetOption().numValue);
        }




        //Menü
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
           
            
        }

        private void öffnenToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog fileAuswahl = new OpenFileDialog();
            string filename;

            //fileAuswahl.FileName = ".voc";
            fileAuswahl.Filter = "Vokabeldatei (*.voc)|*.voc";
            fileAuswahl.ShowDialog();


            if (fileAuswahl.FileName == null || fileAuswahl.FileName == "")
            {

            }

            else
            {
                filename = fileAuswahl.FileName;

                this.CurrentFileName = fileAuswahl.FileName;

                Kontrolle();

                Panel vocPan = Panelerzeugen();

                CreateEntries(ReadFromFile(filename), vocPan, Optionen.GetOption().numValue);
            }
            //throw new NotImplementedException();

          //  öffnenToolStripMenuItem.Click += öffnenToolStripMenuItem_Click;
        }

        private void beendenToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
            //beendenToolStripMenuItem.Click += beendenToolStripMenuItem_Click;
        }

        private void einstellungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Einstellungen eForm = new Einstellungen();

            eForm.ShowDialog();
        }

        
        //Close
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Wollen Sie wirklich den Vokabeltrainer beenden?", "Beenden", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        //Variablen
        public string CurrentFileName;
        public void Kontrolle()
        {
            foreach (Control c in Controls)
            {
                if (c.Name == "vocPan")
                {
                    int indexwert = this.Controls.IndexOf(c);
                    this.Controls.RemoveAt(indexwert);
                }
            }
        }
        public Panel Panelerzeugen()
        {
            Panel vocPan = new Panel();

            vocPan.Name = "vocPan";
            vocPan.Left = 200;
            vocPan.Top = 100;
            vocPan.Height = 350;
            vocPan.Width = 500;
            vocPan.BackColor = Color.White;

            vocPan.AutoScroll = false;
            vocPan.HorizontalScroll.Enabled = false;
            vocPan.HorizontalScroll.Visible = false;
            vocPan.HorizontalScroll.Maximum = 0;
            vocPan.AutoScroll = true;

            return vocPan;
        }



        //Shortcut
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            EventArgs a = new EventArgs();

            if (e.Control && e.KeyCode.ToString() == "O")
            {
                öffnenToolStripMenuItem_Click_1(sender, a);
            }

            if (e.Control && e.KeyCode.ToString() == "P")
            {
                beendenToolStripMenuItem_Click_1(sender, a);
            }

            if (e.Control && e.KeyCode.ToString() == "I")
            {
                einstellungenToolStripMenuItem_Click(sender, a);
            }
        }

    }
}
