﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WindowsFormsApplication1
{
    class Optionen
    {
        public bool isDirty { get; set; }
        public int numValue { get; set; }
        public bool mod { get; set; }

        private static Optionen opt = null;

        private Optionen() { }

        public static Optionen GetOption()
        {
            if (opt == null)
            {
                opt = new Optionen();
            }

            return opt;
         }

        public void ReadOptions()
        {
            StreamReader sr = new StreamReader("config.txt");
            int anzahl = 0;
            string modus = "";
            bool modusB = false;

            while (sr.Peek() != -1)
            {
                string[] s = sr.ReadLine().Split(';');

                anzahl = Convert.ToInt32(s[0]);
                modus = s[1];
            }

            sr.Close();


            if (modus.ToLower() == "true")
            { modusB = true; }
            else
                modusB = false;

            

            if ( numValue != anzahl || mod != modusB)
            {
                isDirty = true;
            }
            else
                isDirty = false;
        }

        public void WriteOptions(bool isDirty)
        {
            bool modusauswahl = false;

            modusauswahl = mod;

            if (isDirty == true)
            {
                StreamWriter sw = new StreamWriter("config.txt");

                sw.WriteLine(numValue + ";" + modusauswahl);

                sw.Flush();
                sw.Close();
            }
            else
            {

            }
        }
    }
}
