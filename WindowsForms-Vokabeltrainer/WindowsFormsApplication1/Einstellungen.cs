﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Einstellungen : Form
    {
        public Einstellungen()
        {
            InitializeComponent();
        }

        private bool isDirty;

        private void speichern_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Das wird erst in der Version 3.0 möglich sein!"); 

            Optionen.GetOption().numValue = Convert.ToInt32(anzahlUpDown.Value);

            if (Convert.ToString(modusBox.SelectedItem) == "EN-DE")
            {
                Optionen.GetOption().mod = true;
            }
            else
                Optionen.GetOption().mod = false;



            Optionen.GetOption().ReadOptions();
            Optionen.GetOption().WriteOptions(Optionen.GetOption().isDirty);

            //ReadOptions();
            //WriteOptions(isDirty);



            this.Close();
        }

        private void abbrechen_Click(object sender, EventArgs e)
        {
           this.Close();
        }
    }
}
