﻿namespace WindowsFormsApplication1
{
    partial class Einstellungen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Einstellungen));
            this.speichern = new System.Windows.Forms.Button();
            this.abbrechen = new System.Windows.Forms.Button();
            this.modusBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.anzahlUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.anzahlUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // speichern
            // 
            this.speichern.Location = new System.Drawing.Point(136, 331);
            this.speichern.Name = "speichern";
            this.speichern.Size = new System.Drawing.Size(129, 42);
            this.speichern.TabIndex = 0;
            this.speichern.Text = "Speichern";
            this.speichern.UseVisualStyleBackColor = true;
            this.speichern.Click += new System.EventHandler(this.speichern_Click);
            // 
            // abbrechen
            // 
            this.abbrechen.Location = new System.Drawing.Point(300, 331);
            this.abbrechen.Name = "abbrechen";
            this.abbrechen.Size = new System.Drawing.Size(129, 42);
            this.abbrechen.TabIndex = 1;
            this.abbrechen.Text = "Abbrechen";
            this.abbrechen.UseVisualStyleBackColor = true;
            this.abbrechen.Click += new System.EventHandler(this.abbrechen_Click);
            // 
            // modusBox
            // 
            this.modusBox.FormattingEnabled = true;
            this.modusBox.Items.AddRange(new object[] {
            "DE-EN",
            "EN-DE"});
            this.modusBox.Location = new System.Drawing.Point(308, 166);
            this.modusBox.Name = "modusBox";
            this.modusBox.Size = new System.Drawing.Size(121, 21);
            this.modusBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(132, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Modus:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(132, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "Anzahl:";
            // 
            // anzahlUpDown
            // 
            this.anzahlUpDown.Location = new System.Drawing.Point(309, 72);
            this.anzahlUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.anzahlUpDown.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.anzahlUpDown.Name = "anzahlUpDown";
            this.anzahlUpDown.Size = new System.Drawing.Size(120, 20);
            this.anzahlUpDown.TabIndex = 5;
            this.anzahlUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // Einstellungen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 396);
            this.Controls.Add(this.anzahlUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.modusBox);
            this.Controls.Add(this.abbrechen);
            this.Controls.Add(this.speichern);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Einstellungen";
            this.Text = "Einstellungen";
            ((System.ComponentModel.ISupportInitialize)(this.anzahlUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button speichern;
        private System.Windows.Forms.Button abbrechen;
        private System.Windows.Forms.ComboBox modusBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown anzahlUpDown;
    }
}