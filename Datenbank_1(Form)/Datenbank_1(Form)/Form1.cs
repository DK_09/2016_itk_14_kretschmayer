﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Datenbank_1_Form_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void speichern_Click(object sender, EventArgs e)
        {
            try
            {
                string conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb";

                OleDbConnection con = new OleDbConnection(conStr);
                con.Open();

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;
                cmd.CommandText = "Insert into buch values("
                    + textBox_buchnr.Text + ", "
                    + textBox_t_id.Text + ", '"
                    + textBox_isbn.Text + "', '"
                    + textBox_titel.Text + "', '"
                    + textBox_autor.Text + "', "
                    + textBox_jahr.Text + ", "
                    + textBox_preis.Text + ")";

                int anz = cmd.ExecuteNonQuery();
                MessageBox.Show("" + anz);

                con.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
    }
}
