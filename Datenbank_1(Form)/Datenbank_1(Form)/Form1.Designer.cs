﻿namespace Datenbank_1_Form_
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.buchnr = new System.Windows.Forms.Label();
            this.themenid = new System.Windows.Forms.Label();
            this.isbn = new System.Windows.Forms.Label();
            this.titel = new System.Windows.Forms.Label();
            this.autor = new System.Windows.Forms.Label();
            this.ejahr = new System.Windows.Forms.Label();
            this.preis = new System.Windows.Forms.Label();
            this.textBox_buchnr = new System.Windows.Forms.TextBox();
            this.textBox_t_id = new System.Windows.Forms.TextBox();
            this.textBox_isbn = new System.Windows.Forms.TextBox();
            this.textBox_titel = new System.Windows.Forms.TextBox();
            this.textBox_autor = new System.Windows.Forms.TextBox();
            this.textBox_jahr = new System.Windows.Forms.TextBox();
            this.textBox_preis = new System.Windows.Forms.TextBox();
            this.speichern = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buchnr
            // 
            this.buchnr.AutoSize = true;
            this.buchnr.Location = new System.Drawing.Point(43, 54);
            this.buchnr.Name = "buchnr";
            this.buchnr.Size = new System.Drawing.Size(69, 13);
            this.buchnr.TabIndex = 0;
            this.buchnr.Text = "Buchnummer";
            // 
            // themenid
            // 
            this.themenid.AutoSize = true;
            this.themenid.Location = new System.Drawing.Point(43, 103);
            this.themenid.Name = "themenid";
            this.themenid.Size = new System.Drawing.Size(57, 13);
            this.themenid.TabIndex = 1;
            this.themenid.Text = "ThemenID";
            // 
            // isbn
            // 
            this.isbn.AutoSize = true;
            this.isbn.Location = new System.Drawing.Point(43, 143);
            this.isbn.Name = "isbn";
            this.isbn.Size = new System.Drawing.Size(32, 13);
            this.isbn.TabIndex = 2;
            this.isbn.Text = "ISBN";
            // 
            // titel
            // 
            this.titel.AutoSize = true;
            this.titel.Location = new System.Drawing.Point(43, 180);
            this.titel.Name = "titel";
            this.titel.Size = new System.Drawing.Size(27, 13);
            this.titel.TabIndex = 3;
            this.titel.Text = "Titel";
            // 
            // autor
            // 
            this.autor.AutoSize = true;
            this.autor.Location = new System.Drawing.Point(43, 225);
            this.autor.Name = "autor";
            this.autor.Size = new System.Drawing.Size(32, 13);
            this.autor.TabIndex = 4;
            this.autor.Text = "Autor";
            // 
            // ejahr
            // 
            this.ejahr.AutoSize = true;
            this.ejahr.Location = new System.Drawing.Point(43, 266);
            this.ejahr.Name = "ejahr";
            this.ejahr.Size = new System.Drawing.Size(88, 13);
            this.ejahr.TabIndex = 5;
            this.ejahr.Text = "Erscheinungsjahr";
            // 
            // preis
            // 
            this.preis.AutoSize = true;
            this.preis.Location = new System.Drawing.Point(43, 311);
            this.preis.Name = "preis";
            this.preis.Size = new System.Drawing.Size(30, 13);
            this.preis.TabIndex = 6;
            this.preis.Text = "Preis";
            // 
            // textBox_buchnr
            // 
            this.textBox_buchnr.Location = new System.Drawing.Point(223, 54);
            this.textBox_buchnr.Name = "textBox_buchnr";
            this.textBox_buchnr.Size = new System.Drawing.Size(100, 20);
            this.textBox_buchnr.TabIndex = 7;
            // 
            // textBox_t_id
            // 
            this.textBox_t_id.Location = new System.Drawing.Point(223, 100);
            this.textBox_t_id.Name = "textBox_t_id";
            this.textBox_t_id.Size = new System.Drawing.Size(100, 20);
            this.textBox_t_id.TabIndex = 8;
            // 
            // textBox_isbn
            // 
            this.textBox_isbn.Location = new System.Drawing.Point(223, 140);
            this.textBox_isbn.Name = "textBox_isbn";
            this.textBox_isbn.Size = new System.Drawing.Size(100, 20);
            this.textBox_isbn.TabIndex = 9;
            // 
            // textBox_titel
            // 
            this.textBox_titel.Location = new System.Drawing.Point(223, 177);
            this.textBox_titel.Name = "textBox_titel";
            this.textBox_titel.Size = new System.Drawing.Size(100, 20);
            this.textBox_titel.TabIndex = 10;
            // 
            // textBox_autor
            // 
            this.textBox_autor.Location = new System.Drawing.Point(223, 222);
            this.textBox_autor.Name = "textBox_autor";
            this.textBox_autor.Size = new System.Drawing.Size(100, 20);
            this.textBox_autor.TabIndex = 11;
            // 
            // textBox_jahr
            // 
            this.textBox_jahr.Location = new System.Drawing.Point(223, 263);
            this.textBox_jahr.Name = "textBox_jahr";
            this.textBox_jahr.Size = new System.Drawing.Size(100, 20);
            this.textBox_jahr.TabIndex = 12;
            // 
            // textBox_preis
            // 
            this.textBox_preis.Location = new System.Drawing.Point(223, 308);
            this.textBox_preis.Name = "textBox_preis";
            this.textBox_preis.Size = new System.Drawing.Size(100, 20);
            this.textBox_preis.TabIndex = 13;
            // 
            // speichern
            // 
            this.speichern.Location = new System.Drawing.Point(128, 376);
            this.speichern.Name = "speichern";
            this.speichern.Size = new System.Drawing.Size(132, 45);
            this.speichern.TabIndex = 14;
            this.speichern.Text = "Speichern";
            this.speichern.UseVisualStyleBackColor = true;
            this.speichern.Click += new System.EventHandler(this.speichern_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 470);
            this.Controls.Add(this.speichern);
            this.Controls.Add(this.textBox_preis);
            this.Controls.Add(this.textBox_jahr);
            this.Controls.Add(this.textBox_autor);
            this.Controls.Add(this.textBox_titel);
            this.Controls.Add(this.textBox_isbn);
            this.Controls.Add(this.textBox_t_id);
            this.Controls.Add(this.textBox_buchnr);
            this.Controls.Add(this.preis);
            this.Controls.Add(this.ejahr);
            this.Controls.Add(this.autor);
            this.Controls.Add(this.titel);
            this.Controls.Add(this.isbn);
            this.Controls.Add(this.themenid);
            this.Controls.Add(this.buchnr);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label buchnr;
        private System.Windows.Forms.Label themenid;
        private System.Windows.Forms.Label isbn;
        private System.Windows.Forms.Label titel;
        private System.Windows.Forms.Label autor;
        private System.Windows.Forms.Label ejahr;
        private System.Windows.Forms.Label preis;
        private System.Windows.Forms.TextBox textBox_buchnr;
        private System.Windows.Forms.TextBox textBox_t_id;
        private System.Windows.Forms.TextBox textBox_isbn;
        private System.Windows.Forms.TextBox textBox_titel;
        private System.Windows.Forms.TextBox textBox_autor;
        private System.Windows.Forms.TextBox textBox_jahr;
        private System.Windows.Forms.TextBox textBox_preis;
        private System.Windows.Forms.Button speichern;
    }
}

